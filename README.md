# Spring boot - Case Study - Online Store

This repository is for the case study of an online store. Following the guidelines of the course of Prof. Dr. Nelio Alves

Course: Spring Boot with Ionic - Complete Case Study
https://www.udemy.com/user/nelio-alves

### Requirement Run
- Docker

### Requirement Dev
- Docker
- Java 1.8

### Spring Boot Config
- Project: _Maven Project_
- Language: _Java_
- Spring Boot: _2.4.4_
- Dependencies: _\[ "Spring Web" \]_
- Packaging: _Jar_
- Java: _8_

### Project Metadata
- Group: _com.matheusmaia.cursomc_
- Artifact: _cursomc_
- Name: _cursomc_
- Description: _Estudo de caso Java Spring para curso de Modelagem Conceitual com UML_
- Package name: _com.matheusmaia.cursomc_

### Create by
- Name: _Matheus da Fonseca Maia de Lima_
- Email: _7matheus.maia@gmail.com_
- GitLab: _https://gitlab.com/MatheusMaia_

### Run MySql and PhpMyAdmin
- Link: _https://medium.com/@mukitulislamratul/how-to-run-springboot-app-container-with-mysql-container-and-phpmyadmin-container-together-using-76b36e1ae283_

#### Config
- Mysql
    - Port: _3306_
    - User: _root_
    - Password: _mukit_

- PhpMyAdmin
    - Port: _8082_
    - User: _root_
    - Password: _mukit_

#### Command for Docker Compose

- Start: ` docker-compose up -d `
- Stop: ` docker-compose down `

#### File _application.properties_ config
- Link: _https://github.com/netgloo/spring-boot-samples/blob/master/spring-boot-mysql-springdatajpa-hibernate/src/main/resources/application.properties_

#### File _pom.xml_ config
- Link: _https://github.com/mukitul/customer-service/blob/master/pom.xml_




