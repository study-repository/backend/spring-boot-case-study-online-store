package com.matheusmaia.cursomc;

import com.matheusmaia.cursomc.domain.*;
import com.matheusmaia.cursomc.domain.enums.TipoCliente;
import com.matheusmaia.cursomc.repositories.CidadeRepository;
import com.matheusmaia.cursomc.repositories.ClienteRepository;
import com.matheusmaia.cursomc.repositories.EnderecoRepository;
import com.matheusmaia.cursomc.repositories.EstadoRepository;
import com.matheusmaia.cursomc.services.CategoriaService;

import com.matheusmaia.cursomc.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.lang.reflect.Array;
import java.util.Arrays;

@SpringBootApplication
public class CursomcApplication implements CommandLineRunner {

	@Autowired
	private CategoriaService categoriaService;
	@Autowired
	private ProdutoService produtoService;
	@Autowired
	private EstadoRepository estadoRepository;
	@Autowired
	private CidadeRepository cidadeRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private EnderecoRepository enderecoRepository;

	public static void main(String[] args) {
		SpringApplication.run(CursomcApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Categoria cat1 = new Categoria(null, "Informatica");
		Categoria cat2 = new Categoria(null, "Escritorio");

		Produto prod1 = new Produto(null,"Caderno", 12.12);
		Produto prod2 = new Produto(null,"Notebook", 222.12);
		Produto prod3 = new Produto(null,"Computador", 100.00);

		cat1.getProdutos().addAll(Arrays.asList(prod1,prod2, prod3));
		cat2.getProdutos().addAll(Arrays.asList(prod1,prod2));

		prod1.getCategorias().addAll(Arrays.asList(cat1, cat2));
		prod2.getCategorias().addAll(Arrays.asList(cat1, cat2));
		prod3.getCategorias().addAll(Arrays.asList(cat1));

		categoriaService.saveAll(Arrays.asList(cat1, cat2));
		produtoService.saveAll(Arrays.asList(prod1, prod2, prod3));

		Estado est1 = new Estado(null, "Minas Gerais");
		Estado est2 = new Estado(null, "São Paulo");

		Cidade c1 = new Cidade(null, "Uberlândia", est1);
		Cidade c2 = new Cidade(null, "São Paulo", est2);
		Cidade c3 = new Cidade(null, "Campinas", est2);

		est1.getCidades().addAll(Arrays.asList(c1));
		est2.getCidades().addAll(Arrays.asList(c2, c3));

		estadoRepository.saveAll(Arrays.asList(est1,est2));
		cidadeRepository.saveAll(Arrays.asList(c1,c2,c3));

		Cliente cli1 = new Cliente(null, "Maria Silva", "maria@gmail.com", "120301230123", TipoCliente.PESSOAFISICA);
		cli1.getTelefones().addAll(Arrays.asList("0123123123","12312312312"));

		Endereco e1 = new Endereco(null, "Rua Flores", "300","Apt 303", "Jardin", "12312312312", cli1, c1);
		Endereco e2 = new Endereco(null, "Avenida Matos", "105","Sala 800", "Centro", "123123", cli1, c2);

		cli1.getEnderecos().addAll(Arrays.asList(e1,e2));

		clienteRepository.saveAll(Arrays.asList(cli1));
		enderecoRepository.saveAll(Arrays.asList(e1, e2));

	}
}
