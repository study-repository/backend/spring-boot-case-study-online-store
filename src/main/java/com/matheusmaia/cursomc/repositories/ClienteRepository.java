package com.matheusmaia.cursomc.repositories;

import com.matheusmaia.cursomc.domain.Cliente;
import com.matheusmaia.cursomc.domain.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

}
