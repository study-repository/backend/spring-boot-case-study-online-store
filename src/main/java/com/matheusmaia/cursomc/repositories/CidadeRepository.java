package com.matheusmaia.cursomc.repositories;

import com.matheusmaia.cursomc.domain.Cidade;
import com.matheusmaia.cursomc.domain.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Integer> {

}
