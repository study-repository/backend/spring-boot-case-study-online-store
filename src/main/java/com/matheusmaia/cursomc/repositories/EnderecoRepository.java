package com.matheusmaia.cursomc.repositories;

import com.matheusmaia.cursomc.domain.Endereco;
import com.matheusmaia.cursomc.domain.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {

}
