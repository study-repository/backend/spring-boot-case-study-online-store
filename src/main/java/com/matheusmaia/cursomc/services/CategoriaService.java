package com.matheusmaia.cursomc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matheusmaia.cursomc.domain.Categoria;
import com.matheusmaia.cursomc.repositories.CategoriaRepository;
import com.matheusmaia.cursomc.services.exceptions.ObjectNotFoundException;


import java.util.List;


@Service
public class CategoriaService {

    @Autowired
    private CategoriaRepository repo;

    public Categoria find(Integer id){
        return repo.findById(id).orElseThrow(
                () -> new ObjectNotFoundException(
                        "Objeto nao encontrado!  Id: "+ id +", Tipo: "+ Categoria.class.getName()));
    }

    public List<Categoria> findAll(){
        return repo.findAll();
    }

    public void saveAll(List<Categoria> categoriaList){
        repo.saveAll(categoriaList);
    }

    public void save(Categoria categoria){
        repo.save(categoria);
    }
}
