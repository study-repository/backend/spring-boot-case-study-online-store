package com.matheusmaia.cursomc.services;

import com.matheusmaia.cursomc.domain.Categoria;
import com.matheusmaia.cursomc.domain.Produto;
import com.matheusmaia.cursomc.repositories.CategoriaRepository;
import com.matheusmaia.cursomc.repositories.ProdutoRepository;
import com.matheusmaia.cursomc.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public void saveAll(List<Produto> produtoList){
        produtoRepository.saveAll(produtoList);
    }

    public void save(Produto produto){
        produtoRepository.save(produto);
    }
}
